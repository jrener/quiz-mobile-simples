package com.example.resial.thequest;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_main);

        //Titulo da barra
        ActionBar actionB = getSupportActionBar();
        actionB.setTitle(getResources().getString(R.string.app_name));


    }

    public void newGame(View view) {
        Intent startGame = new Intent(this, question.class);
        startActivity(startGame);
    }

    public void showChangeLanguageDialog(View view) {
        //Resources: estou definindo esta resource para manter o aplicativo 100% em bilingue
        Resources res = getResources();
        final String[] language = res.getStringArray(R.array.languageOptions);
        AlertDialog.Builder lBuilder = new AlertDialog.Builder( MainActivity.this);
        lBuilder.setTitle(res.getString(R.string.txtvChooseLanguage));
        lBuilder.setSingleChoiceItems(language, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if  (which==0){
                    //Português
                    setLocale("pt");
                    MainActivity.super.recreate();
                    exitApp();
                } else if (which==1){
                    //Inglês
                    setLocale("en");
                    MainActivity.super.recreate();
                    exitApp();
                }

                dialog.dismiss();

            }
        });

        AlertDialog mBuilder = lBuilder.create();
        mBuilder.show();
    }

    private void setLocale(String language) {
        //Configurações do Locale do APP
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //Salvando dados para preferências compartilhadas
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_lang",language);
        editor.apply();
    }

    public void loadLocale(){
        SharedPreferences prefs = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = prefs.getString("My_lang","");
        setLocale(language);
    }

    public void exitApp(){
        moveTaskToBack(true);
        this.finishAffinity();
        }

}
