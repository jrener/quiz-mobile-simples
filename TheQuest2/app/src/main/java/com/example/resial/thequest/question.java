package com.example.resial.thequest;


import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


public class question extends AppCompatActivity {

    Button btNext;
    TextView txtResult;
    TextView txtQuestion;
    RadioGroup  rg;
    RadioButton r1;
    RadioButton r2;
    RadioButton r3;
    RadioButton r4;
    private int questNumber, points, checkRes;
    private String result, questionT;


    String[] question;
    String[] res1;
    String[] res2;
    String[] res3;
    String[] res4;
    int[] correctResPos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Resources res = getResources();

        //-------------------------------------------------------
        //Questões
        //-------------------------------------------------------
        question        =   res.getStringArray(R.array.question);
        res1            =   res.getStringArray(R.array.res1);
        res2            =   res.getStringArray(R.array.res2);
        res3            =   res.getStringArray(R.array.res3);
        res4            =   res.getStringArray(R.array.res4);
        correctResPos   =   res.getIntArray(R.array.correctRes);
        //-------------------------------------------------------
        //Fim Questões
        //-------------------------------------------------------

        //-------------------------------------------------------
        //Definição da interface
        //-------------------------------------------------------
        btNext          =   findViewById(R.id.bttNext);
        txtResult       =   findViewById(R.id.txtvResult);
        txtQuestion     =   findViewById(R.id.txtvQuestion);
        rg              =   findViewById(R.id.rg);
        r1              =   findViewById(R.id.op1);
        r2              =   findViewById(R.id.op2);
        r3              =   findViewById(R.id.op3);
        r4              =   findViewById(R.id.op4);
        questNumber     =   0;
        points          =   0;
        result          =   txtResult.getText().toString();
        questionT       =   txtQuestion.getText().toString();
        //-------------------------------------------------------
        //Fim da definição da interface
        //-------------------------------------------------------

        //-------------------------------------------------------
        //Configurando primeira questão
        //-------------------------------------------------------
        setQuestionAndRes();
        //-------------------------------------------------------
        //Fim do construtor
        //-------------------------------------------------------


    }

    /**
     * Autor: João Rener
     * Data: 19/10/2018
     * Breve descrição: Este método é resposável por chamar a próxima questão, validando se a respostas é a correta e
     * atribuindo os pontos.
     * Dependências: checkAnswer() / setPoints() / setQuestionAndRes()
     *
     * @param view view passada como parâmetro para configuração
     */
    public void nextQuestion(View view) {

        checkAnswer();
        setPoints();
        questNumber++;
        setQuestionAndRes();
        rg.clearCheck();

    }
    /**
     * Autor: João Rener
     * Data: 19/10/2018
     * Breve descrição: Método responsavel por mudar as questões e respostas para as seguintes e caso tenha acabado
     * ele desativa o botão de próximo e informa ao usuário que as questões acabaram.
     * Dependências: Não possui.
     */
    public void setQuestionAndRes(){
        if(questNumber<question.length){
            txtResult.setText(result + " " + points);
            txtQuestion.setText(questionT + question[questNumber]);
            r1.setText(res1[questNumber] + "");
            r2.setText(res2[questNumber] + "");
            r3.setText(res3[questNumber] + "");
            r4.setText(res4[questNumber] + "");
        }
        else {
            txtQuestion.setText(getResources().getString(R.string.txtEndQ));
            btNext.setEnabled(false);
            r1.setText("");
            r2.setText("");
            r3.setText("");
            r4.setText("");
        }
    }
    /**
     * Autor: João Rener
     * Data: 19/10/2018
     * Breve descrição: Aumenta os pontos do usuário caso acerte a questão.
     * Dependências: Não possui.
     */
    public void setPoints(){
        if(checkRes==correctResPos[questNumber]){
            points++;
            txtResult.setText(result + " " + points);
        }
    }
    /**
     * Autor: João Rener
     * Data: 19/10/2018
     * Breve descrição: Verifica se a resposta marcada no RadioButton é a correta.
     * Dependências: Não possui.
     */
    public void checkAnswer(){
        if(r1.isChecked()){
            checkRes=1;
        }else if(r2.isChecked()){
            checkRes=2;
        }else if(r3.isChecked()){
            checkRes=3;
        }else if(r4.isChecked()){
            checkRes=4;
        }else
            checkRes=0;
    }

}

