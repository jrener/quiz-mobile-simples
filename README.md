# TheQuest2

Aplicativo simples desenvolvido com 2 idiomas (Português / Inglês) como exercício prático da aula de Desenvolvimento para dispositivos móveis 
o qual possui uma interface simples e algumas questões para serem respondidas, contabilizando o número de questões respondidas corretamente.


## Pre Requisitos

*   Java 1.8 SDK
*   Android Studio
*	Android 4.1 (IceCream) ou superior

## AUTORES

* **João Rener** 


